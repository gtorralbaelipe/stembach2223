numpy>=1.20
matplotlib>=3.4
pandas>=1.2
tensorflow>=2.8
astropy>=4.0
